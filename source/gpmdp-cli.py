#!/usr/local/bin/python3

import sys
import json
import logging
import argparse

from collections import defaultdict

import asyncio
import websockets

logger = logging.getLogger(__file__)


def _get_connect_msg(args=None):
    return {
        "namespace": "connect",
        "method": "connect",
        "arguments": ["GMDP Python Control"] + (args or [])
    }


class AlfredSearchResults:
    def __init__(self):
        self.items = []

    def add_artist(self, artist):
        self.items.append({
            "title": artist['name'],
            "valid": True,
            "arg": json.dumps(artist)
        })

    def add_album(self, album):
        self.items.append({
            "title": album['name'],
            "subtitle": album['artist'],
            "valid": True,
            "arg": json.dumps(album)
        })

    def add_track(self, track):
        self.items.append({
            "title": track['title'],
            "subtitle": "%(artist)s - %(album)s" % track,
            "valid": True,
            "arg": json.dumps(track)
        })

    @property
    def alfred_results(self):
        return {"items": self.items}

    def __str__(self):
        return json.dumps(self.alfred_results)


class GPMDPControl:

    def __init__(self, url, appkey=None, queue_depth=10):
        self.queue_depth = queue_depth
        self.queues = defaultdict(asyncio.Queue)
        self.sock = None
        self.recv_task = None
        self.url = url
        self.appkey = appkey
        self.req_counter = 1
        self.init_cond = asyncio.Condition()

    def _get_req_id(self):
        cur = self.req_counter
        self.req_counter += 1
        return cur

    async def _wait_for_resp(self, reqId):
        while True:
            rsp = await self.queues["result"].get()
            if rsp.get("requestID", 0) == reqId:
                return rsp

    async def _send_msg(self, msg):
        logger.debug("Sending %s" % msg)
        await self.sock.send(msg if msg is str else json.dumps(msg))

    async def _cmd_resp(self, msg, recv_channel):
        await self._send_msg(msg)
        return await self.queues[recv_channel].get()

    async def _rpc_call(self, namespace, method, *args, **kwargs):
        msg = {"namespace": namespace, "method": method, "arguments": args}
        msg.update(kwargs)
        await self._send_msg(msg)

    async def _rpc_call_rsp(self, namespace, method, *args):
        reqId = self._get_req_id()
        await self._rpc_call(namespace, method, *args, requestID=reqId)
        resp = await self._wait_for_resp(reqId)
        return resp.get("value")

    async def _get_appkey(self):
            rsp = await self._cmd_resp(_get_connect_msg(), "connect")
            print(str(rsp))

            if rsp["payload"] == "CODE_REQUIRED":
                pin = input("Input 4 digit pin displayed by GMDP: ")
                rsp = await self._cmd_resp(_get_connect_msg([pin.strip()]), "connect")

                if rsp["payload"] != "CODE_REQUIRED":
                    logger.debug("Successfully connected, using key %s" % rsp["payload"])
                    return rsp["payload"]
            else:
                logger.error("Unknown message received: %s" % rsp)

    async def _recvdata(self):
        logger.debug("Starting receive loop")
        while self.sock:
            resp = await self.sock.recv()

            msg = json.loads(resp)
            q_name = msg.get("channel") or msg.get("namespace")

            logger.debug("Received message for queue %s" % q_name)
            if not q_name:
                logger.warning("Don't know what to do with %s" % msg)

            q = self.queues[q_name]
            while q.qsize() >= self.queue_depth:
                _ = q.get_nowait()

            await q.put(msg)

    async def _isPlaying(self):
        logger.debug("Checking whether GPMDP is currently playing")
        isPlaying = await self._rpc_call_rsp("playback", "isPlaying")
        logger.debug("isPlaying() returned %s", isPlaying)
        return bool(isPlaying)

    async def init(self):
        # Ensure the connection is fully initialized
        async with self.init_cond:
            if not self.recv_task:
                logger.info("Connecting to %s" % self.url)
                self.sock = await websockets.connect(self.url, max_size=2**23)
                # "Launch" the background socket receive task
                self.recv_task = asyncio.ensure_future(self._recvdata())

                logger.debug("Waiting for initial message from server")
                rsp = await self.queues["API_VERSION"].get()
                if not self.appkey:
                    self.appkey = await self._get_appkey()

                await self._send_msg(_get_connect_msg([self.appkey]))
                logger.info("Authenticated: %s" % rsp)
                self.init_cond.notify_all()

    async def stop(self):
        if self.recv_task:
            self.recv_task.cancel()
            self.recv_task = None

        if self.sock:
            await self.sock.close()
            self.sock = None

    #
    # Actual action methods
    #
    async def current_track(self, alfred_json):
        await self.init()
        curTrack = await self._rpc_call_rsp("playback", "getCurrentTrack")
        if alfred_json:
            isPlaying = await self._isPlaying()
            print(json.dumps({"items": [
                {"title": "%s => %s" % ("Pause" if isPlaying else "Play",
                                        curTrack.get("title")),
                 "subtitle": "%s - %s" % (curTrack.get("artist"), curTrack.get("album")),
                 "arg": "-"}
            ]}))
        else:
            print(curTrack)

    async def current_rating(self):
        await self.init()
        curRating = await self._rpc_call_rsp("rating", "getRating")
        print("== Current Rating ==\n%s" % curRating)
        return curRating

    async def current_queue(self, alfred_json):
        await self.init()
        logger.debug("Get current queue tracks")
        tracks = await self._rpc_call_rsp("queue", "getTracks")
        if alfred_json:
            items = []
            for track in tracks:
                items.append({
                    "title": track.get('title'),
                    "subtitle": "%(artist)s - %(album)s" % track,
                    "valid": True,
                    "arg": json.dumps(track)
                })
            print(json.dumps({"items": items}))
        else:
            print("== Current Queue")
            for track in tracks:
                print("%s\n  %s - %s" % (track.get('title'),
                                         track.get('artist'),
                                         track.get('album')))

    async def play_queue(self, track):
        await self.init()
        logger.debug("Play track %s", track)
        rsp = await self._rpc_call_rsp("queue", "playTrack", json.loads(track))
        logger.debug("Play track response: %s", rsp)

    async def like_current_song(self, like):
        await self.init()
        await self._rpc_call("rating", "setRating", like and 5 or 1)
        await self.queues["rating"].get()
        curRating = await self.current_rating()
        return curRating

    async def playpause(self, forceState=None):
        await self.init()
        if forceState is None or await self._isPlaying() != forceState:
            await self._rpc_call("playback", "playPause")

    async def next(self):
        await self.init()
        await self._rpc_call("playback", "forward")

    async def prev(self):
        await self.init()
        await self._rpc_call("playback", "rewind")

    async def search(self, search_arg, alfred_json, limit=[]):
        await self.init()
        rsp = await self._rpc_call_rsp("search", "performSearch", search_arg)
        if alfred_json:
            alfred_res = AlfredSearchResults()

            best = rsp.get("bestMatch")
            if best:
                match_type = best.get("type")
                if not limit or match_type in limit:
                    func = getattr(alfred_res, "add_" + match_type)
                    func(best.get("value"))

            if not limit or "artist" in limit:
                for artist in rsp.get("artists", []):
                    alfred_res.add_artist(artist)

            if not limit or "album" in limit:
                for album in rsp.get("albums", []):
                    alfred_res.add_album(album)

            if not limit or "track" in limit:
                for track in rsp.get("tracks", []):
                    alfred_res.add_track(track)

            print(alfred_res)

        else:
            print("Search Response:")
            # Albums
            print("Artists:")

            for artist in rsp.get("artists", []):
                print(f"  {artist['name']}")

            print("Albums:")
            for album in rsp.get("albums", []):
                print(f"  {album['artist']} - {album['name']}")

            print("Tracks:")
            for track in rsp.get("tracks", []):
                print(f"  {track['artist']} - {track['album']}")
                print(f"  {track['title']} - {track['album']}")

    async def play_result(self, result):
        await self.init()
        logger.debug("Playing %s", result)
        rsp = await self._rpc_call("search", "playResult", json.loads(result))


if __name__ == "__main__":

    logging.basicConfig(stream=sys.stderr)

    parser = argparse.ArgumentParser(description="Control Google Music Desktop Player")

    # Global Options
    parser.add_argument("--verbose", "-v", default=False, action="store_true",
                        help="Output verbose debug info")

    parser.add_argument("--url", "-U", help="URL to reach the Google Music Desktop Player",
                        default='ws://localhost:5672')

    parser.add_argument("--app-key", "-K", help="Application key to use for this connection",
                        default="2ffc56d4-9e17-47ac-aa27-f0ebbfe76a0d")

    subparsers = parser.add_subparsers(dest="cmd", help="subcommand help")

    # play
    parser_play = subparsers.add_parser("play", help="Start play the current queue")

    # pause
    parser_pause = subparsers.add_parser("pause", help="Pause playback of the current song")

    # playpause
    parser_playpause = subparsers.add_parser("playpause", help="Toggle playing the current song")

    # next
    parser_next = subparsers.add_parser("next", help="Skip to the next song")

    # prev
    parser_prev = subparsers.add_parser("prev", help="Skip to the previous song")

    # like
    parser_like = subparsers.add_parser("like", help="Like the current song")

    # dislike
    parser_dislike = subparsers.add_parser("dislike", help="Dislike the current song")

    # current track
    parser_current = subparsers.add_parser("current", help="Get the current track")

    parser_current.add_argument("--alfred-json", "-J", default=False, action="store_true",
                                help="Output results in the alfred json format")

    # queue
    parser_queue = subparsers.add_parser("queue", help="Get the current play queue")

    parser_queue.add_argument("--alfred-json", "-J", default=False, action="store_true",
                              help="Output results in the alfred json format")

    # playqueue
    parser_playqueue = subparsers.add_parser("playqueue", help="Play a specific track in the queue")

    parser_playqueue.add_argument("track", type=str,
                                  help="The track returned from 'queue'")
    # search
    parser_search = subparsers.add_parser("search", help="Search the current song")

    parser_search.add_argument("--artist", "-A", action="store_true",
                               help="Limit search to matching Artists")

    parser_search.add_argument("--album", "-L", action="store_true",
                               help="Limit search to matching Albums")

    parser_search.add_argument("--track", "-T", action="store_true",
                               help="Limit search to matching tracks")

    parser_search.add_argument("--library", "-B", action="store_true",
                               help="Limit search to your library.")

    parser_search.add_argument("--alfred-json", "-J", default=False, action="store_true",
                               help="Output results in the alfred json format")

    parser_search.add_argument("terms", nargs="+",
                               help="The terms to search for")

    # playresult
    parser_playresult = subparsers.add_parser("playresult",
                                              help="Play a result returned from a previous search")

    parser_playresult.add_argument("result", type=str,
                                   help="The result as returned from a previous search")

    opts = parser.parse_args()

    if not opts.cmd:
        sys.stdout.write("Include a subcommand\n")
        parser.print_usage()
        sys.exit(1)

    if opts.verbose:
        logger.setLevel(logging.DEBUG)

    async def run_command(opts, parser):
        logger.info("Using %s with appkey %s" % (opts.url, opts.app_key))
        ctrl = GPMDPControl(opts.url, appkey=opts.app_key)

        if opts.cmd == "playpause":
            await ctrl.playpause()
        elif opts.cmd == "play":
            await ctrl.playpause(True)
        elif opts.cmd == "pause":
            await ctrl.playpause(False)
        elif opts.cmd == "current":
            await ctrl.current_track(opts.alfred_json)
        elif opts.cmd == "queue":
            await ctrl.current_queue(opts.alfred_json)
        elif opts.cmd == "playqueue":
            await ctrl.play_queue(opts.track)
        elif opts.cmd == "next":
            await ctrl.next()
        elif opts.cmd == "prev":
            await ctrl.prev()
        elif opts.cmd == "like":
            await ctrl.like_current_song(True)
        elif opts.cmd == "dislike":
            await ctrl.like_current_song(False)
        elif opts.cmd == "search":
            limit = []
            if opts.artist:
                limit.append("artist")
            if opts.album:
                limit.append("album")
            if opts.track:
                limit.append("track")

            await ctrl.search(' '.join(opts.terms),
                              alfred_json=opts.alfred_json,
                              limit=limit)
        elif opts.cmd == "playresult":
            await ctrl.play_result(opts.result)
        else:
            sys.stderr.write(f"Unknown subcommand {opts.cmd}\n")
            parser.print_usage(sys.stderr)

        await ctrl.stop()

    asyncio.get_event_loop().run_until_complete(run_command(opts, parser))
